import webapp
import urllib.parse

Formulario = "<form action='' method='POST'>" \
               "<p>" \
               "Url: <input type='text' name='url' size='50'>" \
               "</p>" \
               "<p>" \
               "Url shortened: <input type='text' name='short' size='50'>" \
               "</p>" \
               "<p>" \
               "<input type='submit' value='Enviar'>" \
               "</p>" \
               "</form>"
url_base = "http://localhost:1234/"

class Shortener (webapp.webApp):
#Diccionario
    Urldict = {}

    def parse(self, request):
        method = request.split(' ', 2)[0]
        resource = request.split(' ', 2)[1]
        body = request.split('\r\n\r\n',2)[1]

        return (method,resource,body)

    def process(self, parsedRequest):

        method,resource,body = parsedRequest
        if resource == "/":
            if method == "GET":
                httpCode = "200 OK"
                htmlBody = ("<html><body><h1>" + Formulario + "<h1>List:<br></h1>"
                            + str(self.Urldict) + "</h1></body></html>")

            elif method == "POST":
                url = urllib.parse.unquote(body.split("&")[0].split("=")[1])
                short = body.split("&")[1].split("=")[1]
                if url == "" or short == "":
                    httpCode = "404 Not found"
                    htmlBody = ("<html><body><h1>" + "<h1>Url no existente</h1>"
                                + "</body></html>")
                else:
                    if not url.startswith('http://') and not url.startswith('https://'):
                        url = "https://" + url
                self.Urldict[short] = url
                httpCode = "200 OK"
                htmlBody = f"<html><body>URL Original: <a href='{url}'>{url}</a>" \
                           f"<br>Url Acortada: <a href='{short}'>{url_base + short}</a></body></html>"
        else:
            recurso = resource.split('/')[1]
            if recurso in self.Urldict:
                url = self.Urldict[recurso]
                httpCode = "301 Move permanently"
                htmlBody = ("<html><body>" + "<meta http-equiv='refresh' content='0; URL=" + str(
                    url) + "'></body></html>")
            else:
                httpCode = "404 Not found"
                htmlBody = ("<html><body><h1>" + "<h1>Url no existente</h1>"
                            + "</body></html>")

        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = Shortener("localhost", 1234)




